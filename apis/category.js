export const category = [{
        id: 1,
        title: "Computer",
        description: "All about computers"
    },
    {
        id: 2,
        title: "Kitchen",
        description: "All about kitchen"
    },
    {
        id: 3,
        title: "Clothing",
        description: "All about clothing"
    },
    {
        id: 4,
        title: "Beverages",
        description: "All about beverages"
    }
]